Crowfunding
===========

Para compilar el codigo usar el script build.sh

> ./build.sh

Para ejecutarlo seguir usando el script

> ./build.sh run

No tocar "_shake" por que esta el ejecutable mágico que lo hace casi todo

Se pueden incluir tantos cpp como se quiera, agruparlos en directorios, siempre y
cuando (jamas, nunca) se incluya un cpp, solo hpp.

> #include <file.cpp> // esta prohibido!!!!

No hacer dos main(no lo compila bien) tampoco en caso de necesitarlo, se debería 
trabajar en una rama diferente(con esto me refiero a un uso correcto de git XD)


GIT
===

Si algún interezado en git y su funcionamiento puede buscar manuales o esperar
a tercero XD. Yo lo explicare por encima, git sirve para llevar un control sobre
los cambios que suceden al codigo, basicamente tu tienes unos archivos
modificas algo en especifico y le pongo una etiqueta(commit), con la descripcion de que
he hecho, "añadido un sistema fantastico de commentarios" y ya esta, en caso de
que el "sistema" tenga "fallitos" se puede retroceder al estado anterior y ver
como arreglarlo.

Ademas este tipo de control de cambios permite ver las diferencias de una 
etiqueta(commit) a otra. (En git hay un comando terminal pero no me acuerdo)
Si se sube en una plaforma como github.com o bitbucket.com se pueden esos cambios.

Para mirar el estado actual del proyecto es decir todos los archivos modificados
usar:
> git status

Este indicara que archivos estan modificados, son nuevos o se han eliminado.

Para hacer la supuesta "etiqueta"(commit) añadimos primeros los archivos tocados.
> git add ruta_file_modificado
> git add ruta_dir // este añade todos los cambios del directorio

Esto descarta un cambio respecto ante el commit ultimo
> git checkout -- file_que_no_he_modificado_o_que_no_tiene_nada_que_ver

Finalmente, le damos el nombre!!!!
> git commit -m "mi primer commit debe estar relacionado con lo que hago en el codigo fuente"

Porque si no hay quien lo entienda que tocaste o para que, decir solucione tal bug
puede ayudar mucho XD.


Para caracteristicas experimentales es divertido crear ramas, ademas de la por defecto
"master", (si es por eso que en la terminal lo pone) la cosa es que aunque se
hacer cambios en el proyecto hacia atras con lo anterior puede que se esten modificando
cosas a la ves y volverse todo un lio. Para ellos te crear tu rama haces lo que
quieras en ella y terminas añadiendolo a la principal.


TODO: explicar esto o "git help branch"
> git branch name_branch
> git checkout name_branch
