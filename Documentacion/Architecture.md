Clases Abstractas
=================

En "../Utils/". Tengo definidas una serie de clases con el fin de hacer más
ortogonal la api, menos variaciones sobre el codigo. Como a todo el mundo le
gusta poner sus nombres. Esto da un poco de coordinación.

La clase "MenuAbstract", como su nombre indica, trata los menúes. Si la clase
va necesitar un menú de opciones. Debe heredar. E implementar el método
"menu", con el while común de los menúes. En cambio, el método "menuShowUp"
es un wrap de menu para poder controlar todos los menúes a la vez.
Si se quiere llamar un menú usen "menuShowUp".


La clase FromFile y ToFile son clases para poder mantener un estado persistente
en el programa. Y no perder toda la información almacenada durante la ejecución
