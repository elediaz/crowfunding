#pragma once

#include <string>
#include <iostream>

class MenuAbstract {
private:
  string name_;
public:
  MenuAbstract(string nameClass): name_(nameClass) {};
  ~MenuAbstract() {};
  virtual void menu() = 0;
  virtual void menuShowUp() {
    cout << name_ << endl;
    menu();
    // system clear ....
  };
};
