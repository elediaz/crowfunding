#pragma once
#include <string>
#include <vector>
#include "Proyecto.hpp"

using namespace std;

class Proyecto;

// Debe heredar de MenuAbstract, ToFile y FromFile
class ConjuntoProyectos
{
public:
	vector<Proyecto*> proyecto;

	Proyecto * buscarProyecto(string name);

	void aniadirProyecto(Proyecto pro);

	void eliminarProyecto(Proyecto pro);
	Proyecto * selecionarProyecto();
};
