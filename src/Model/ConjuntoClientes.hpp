#pragma once

#include <vector>
#include <string>
//#include "../common.hpp"
#include "Cliente.hpp"


using namespace std;

// Debe heredar de FromFile y ToFile
class ConjuntoClientes
{
private:
	vector<Cliente *> cliente;

public:
	ConjuntoClientes(void);
	Cliente * getCliente(string name, string pass);

	void addCliente(void);

	void eliminarCliente(int id);
};

