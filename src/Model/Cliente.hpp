#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Proyecto.hpp"
using namespace std;

class Proyecto;


class Cliente {
public:
    string username;
    string password;
    string email;
    vector <Proyecto*> proyectos;
    //Cliente guarda un vector de punteros a sus proyectos
    static int idCount;
    int id;
public:
  Cliente (string nombre, string pass, string email);
  string getUsername (void);
  string getPassword (void);
  string getEmail (void);
  int getId (void);
  void addProyecto (Proyecto* proyecto_);
  void eliminarProyecto (Proyecto* proyecto_); //Cliente decide
  bool perteneceProyecto (Proyecto* proyecto_);
};
