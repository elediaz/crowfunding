#pragma once
#include <string>
#include <iostream>
#include <vector>
#include "Fecha.hpp"
#include "Cliente.hpp"

using namespace std;


class Cliente;

// | Esta clase tiene que heredar de ToFile, FromFile y MenuAbstract
class Comentario {
private:
  static int idCount_;
  // ID contador
  int id_;
  string mensaje_;
  Fecha fecha_;
  Cliente * cliente_;
  vector<Comentario *> comentarios_;
  // ^ Respuestas al comentario, es importante mandar una notificacion
    //   al cliente del comentario de que se le ha respondido
    //   cuando haiga un sistema de notificaciones

    void showComentario(ostream & os, Comentario * comentario, int level);
    // ^ funcion auxiliar para imprimir los comentarios con una indentacion

public:
  Comentario(string mensaje, Cliente * cliente);
  ~Comentario();
  void responderComentario();
  bool comprobarLenguaje();
  friend ostream& operator <<(ostream& os, Comentario& comentario);
};
