#pragma once
#include <string>
#include <vector>
//#include "Innovador.hpp"
#include "Cliente.hpp"
#include "Comentario.hpp"
#include "CuentaBancaria.hpp"
#include "Fecha.hpp"

using namespace std;


// Los diferentes estados en los que se puede encontrar un proyecto
enum Estado{
	Iniciado,
	// ^ Se acaba de crear
	Finalizado,
	// ^ Se llevo acabo la recaudacion con exito
	Cancelado
	// ^ No se alcanzo la recaudacion necesaria o lo cancelo un admin
};

class Cliente;
class Comentario;


class Proyecto {
private:
	int id_;
	// ^ identificador único
	string nombre_;
	string descripcion_;
	Fecha fechainicio_;
	Fecha fechaLimite_;
	CuentaBancaria * cuentaAsociada_;
	Cliente * creadorProyecto_;
	Estado estado_;
	float dineroMax_;
	vector<Comentario *> comentarios_;
	static int countID_;
	// ^ contador global que cuenta por que numero del proyecto vamos

public:

	Proyecto(string nombre, string descripcion, Fecha fLimite, Cliente * creador, float dineroMax);
	~Proyecto(void);
	void PonerComentario(Comentario * comentario);
	void DarDinero(float dinero);
	void CambiarCuenta(CuentaBancaria *a);
};

