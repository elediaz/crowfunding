#include "Cliente.hpp"


Cliente::Cliente(string name = "", string pass = "", string email = ""):
  username(name), password(pass), email(email) {}


string Cliente::getUsername(void) {
  return username;
}

string Cliente::getPassword(void) {
  return password;
}

string Cliente::getEmail(void) {
  return email;
}

int Cliente::getId(void) {
  return id;
}

void Cliente::addProyecto(Proyecto * proyecto_) {
  proyectos.push_back( proyecto_ );
}

void Cliente::eliminarProyecto(Proyecto * proyecto_) {
  unsigned int i;
  for (i=0; i<proyectos.size(); i++)
    if ( proyectos[i] == proyecto_ ) //Comparo direcciones
      break;                       //de memoria

  if (i<proyectos.size()){
    delete proyectos[i]; //Invoca destructor proyecto
    //proyectos.erase(i); // no existe erase
  }
  else cout << "No existe proyecto" << endl;
}

bool Cliente::perteneceProyecto(Proyecto * proyecto_) {
  unsigned int i;
  for (i=0; i<proyectos.size(); i++)
    if ( proyectos[i] == proyecto_ )
      break;
  return ( ( i<proyectos.size() ) ? true : false);
}

