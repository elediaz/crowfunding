#include "Proyecto.hpp"

int Proyecto::countID_;

Proyecto::Proyecto(string nombre, string descripcion, Fecha fLimite, Cliente * creador, float dineroMax):
id_(0),
nombre_(nombre),
descripcion_(descripcion),
fechainicio_(),
fechaLimite_(fLimite),
creadorProyecto_(creador),
estado_(Iniciado),
dineroMax_(dineroMax),
comentarios_()
{
	countID_++;
  id_ = countID_;
}

Proyecto::~Proyecto() {}

void Proyecto::PonerComentario(Comentario * comentario){
    comentarios_.push_back(comentario);
}

void Proyecto::DarDinero(float dinero) {
    switch (estado_) {
        case Iniciado:
            break;
        case Finalizado:
            break;
        case Cancelado:
            break;
    }
}

void Proyecto::CambiarCuenta(CuentaBancaria * a) {
}
