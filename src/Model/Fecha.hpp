#pragma once
#include <string>
#include <iostream>
#include <ctime>

using namespace std;

class Fecha {
private:
  void currentDateTime();

  int year_;
  int month_;
  int day_;

public:
  Fecha();
  Fecha(int year, int month, int day);
  int year(void);
  int month(void);
  int day(void);
  friend ostream& operator<<(ostream & os, const Fecha& fecha);
};
