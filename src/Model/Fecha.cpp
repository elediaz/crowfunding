#include "Fecha.hpp"

void Fecha::currentDateTime() {
  // http://stackoverflow.com/questions/997946/how-to-get-current-time-and-date-in-c
  time_t t = time(0);   // get time now
  struct tm * now = localtime( & t );
  year_  = now->tm_year + 1900;
  month_ = now->tm_mon + 1;
  day_   = now->tm_mday;
}

Fecha::Fecha() {
  currentDateTime();
}


Fecha::Fecha(int year, int month, int day):
  year_(year),
  month_(month),
  day_(day) {}

int Fecha::year() {
  return year_;
}

int Fecha::month() {
  return month_;
}

int Fecha::day() {
  return day_;
}

ostream & operator<<(ostream & os, const Fecha& fecha) {
  // show date
  return os;
}
