#pragma once
#include "RegistroCliente.hpp"
#include "InicioSesion.hpp"
#include "../Utils/MenuAbstract.hpp"


class UserInterface : public MenuAbstract
{
private:
	RegistroCliente registroCliente;
	InicioSesion inicioSesion;

public:
  UserInterface(): MenuAbstract("Pagina de Inicio") {};
  void menu(){
    int opcion=100;
    cout<<"Bienvenido a nuestra pagina de crowfounding"<<endl;
    while(opcion!=0){
      cout<<"¿Que accion desea realizar?"<<endl;
      cout<<"[0]"<<"Salir"<<endl;
      cout<<"[1]"<<"Registrarse"<<endl;
      cout<<"[2]"<<"Inicio Sesion"<<endl;
      cin>>opcion;
      switch(opcion){
      case 0:
        break;
      case 1:
        registroCliente.menuShowUp();
        break;
      case 2:
        inicioSesion.menuShowUp();
        break;
      default:
        cout<<"Opcion no encontrada"<<endl;
      }
    }
  }
};

