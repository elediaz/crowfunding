#pragma once
#include <string>
#include "../Model/Cliente.hpp"
#include "../Model/ConjuntoProyectos.hpp"
#include "../Utils/MenuAbstract.hpp"


using namespace std;

class NavegacionProyectos : MenuAbstract
{
private:
  Cliente * cliente;
	ConjuntoProyectos conjuntoProyectos;
public:
  NavegacionProyectos(Cliente * client):
    MenuAbstract("Busquedad de proyectos"),
    cliente(client) {};
  void menu() {
    cout << "Selecion la forma de busqueda" << endl;
    // busqueda por nombre
    // o fecha...
  };
	void buscarProyecto(string name) {};
};
