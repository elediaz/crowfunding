#pragma once
#include <string>
#include "../Model/Cliente.hpp"
#include "../Model/ConjuntoClientes.hpp"
#include "../Utils/MenuAbstract.hpp"
#include "NavegacionProyectos.hpp"
#include "GestionarProyecto.hpp"


using namespace std;

class InicioSesion : public MenuAbstract
{
private:
  Cliente * cliente;
  NavegacionProyectos navegacionProyectos;
  GestionarProyecto gestionarProyecto;
	ConjuntoClientes * conjuntoClientes;
public:
	InicioSesion(void):
    MenuAbstract("Pagina Principal"),
    cliente(nullptr),
    navegacionProyectos(cliente),
    gestionarProyecto(cliente) {};
  void menu() {
    cout << "Introduzca sus credenciales" << endl;
    string name, pass;
    cout << "Introduzca su nombre" << endl;
    cin >> name;
    cout << "Introduzca su contraseña" << endl;
    cin >> pass;
     cliente = conjuntoClientes->getCliente(name, pass);
    if (cliente !=nullptr) {
      cout << "Se ha registrado correctamente" << endl;
      menu2();
    }
    else {
      cout << "Desea salir del login(y/n)" << endl;
      char ch;
      cin >> ch;
      if (ch == 'n')
        menu();
    }
  }

  void menu2() {
    cout << "¿Qué desea hacer?" << endl;
    cout << "1 - Navegar entre los proyectos activos" << endl;
    cout << "2 - Gestionar sus propios proyectos" << endl;
    cout << "3 - Salir" << endl;
    char aux;
    cin >> aux;
    switch (aux) {
    case '1': /*navegacionProyectos.menuShowUp();*/ break;
    case '2': /*gestionarProyecto.menuShowUp();*/ break;
    case '3': break;
    default: menu2(); break;
    }
  }
};

